using System;
using UnityEngine;


public class MatchMaker
{
    #region Client commands

    /// <summary>
    /// Find match
    /// </summary>
    /// <param name="rank">Lobby rank</param>
    /// <param name="bet">Lobby bet</param>
    /// <param name="score">Lobby score</param>
    /// <param name="nickName">Player nickname</param>
    /// <param name="idPlayer">Player unique id</param>
    /// <param name="platform">Player runtime platform</param>
    /// <param name="callback">Callback</param>
    public static void FindMatch(int rank, int bet, int score, string nickName, string idPlayer, RuntimePlatform platform, Action<LobbyModel> callback)
    {
        ClientRequest.Request(RequestType.find_match, callback, rank, bet, score, nickName, idPlayer, (int)platform);
    }

    /// <summary>
    /// Get all lobbies from match maker
    /// </summary>
    /// <param name="platform">Runtime platform</param>
    public static void GetLobbies(RuntimePlatform platform, Action<LobbiesModel> callback)
    {
        ClientRequest.Request(RequestType.get_lobbies, callback, (int)platform);
    }

    /// <summary>
    /// Connect to lobby
    /// </summary>
    /// <param name="nickName">Player nickname</param>
    /// <param name="idPlayer">Player unique id</param>
    /// <param name="platform">Player runtime platform</param>
    /// <param name="idLobby">Lobby id</param>
    /// <param name="pass">Lobby password</param>
    /// <param name="callback">Callback</param>
    public static void ConnectToLobby(string nickName, string idPlayer, RuntimePlatform platform, int idLobby, string pass, Action<LobbyModel> callback)
    {
        ClientRequest.Request(RequestType.connect_to_lobby, callback, nickName, idPlayer, (int)platform, idLobby, pass);
    }

    /// <summary>
    /// Disconnect from lobby
    /// </summary>
    /// <param name="idPlayer">Player id</param>
    /// <param name="idLobby">Lobby id</param>
    /// <param name="callback">Callback</param>
    public static void DisconnectFromLobby(string idPlayer, int idLobby, Action<Responce> callback)
    {
        ClientRequest.Request(RequestType.disconnect_from_lobby, callback, idPlayer, idLobby);
    }

    /// <summary>
    /// Create custom lobby
    /// </summary>
    /// <param name="rank">Player rank</param>
    /// <param name="bet">Lobby bet</param>
    /// <param name="score">Lobby score</param>
    /// <param name="nickName">Player nickname</param>
    /// <param name="idPlayer">Player unique id</param>
    /// <param name="platform">Player runtime platform</param>
    /// <param name="pass">Lobby password</param>
    /// <param name="callback">Callback</param>
    public static void CreateLobby(int rank, int bet, int score, string nickName, string idPlayer, RuntimePlatform platform, string pass, Action<LobbyModel> callback)
    {
        if (pass == "")
            pass = null;
        ClientRequest.Request(RequestType.create_lobby, callback, rank, bet, score, nickName, idPlayer, (int)platform, pass);
    }

    /// <summary>
    /// Get lobby data by id
    /// </summary>
    /// <param name="lobbyId">Lobby id</param>
    /// <param name="platform">Runtime platform</param>
    /// <param name="callback">Callback</param>
    public static void GetLobbyData(int lobbyId, RuntimePlatform platform, Action<LobbyModel> callback)
    {
        ClientRequest.Request(RequestType.get_lobby_data, callback, lobbyId, (int)platform);
    }

    #endregion

    #region Server commands

    /// <summary>
    /// When room is destroyed, need to release port
    /// </summary>
    /// <param name="port">Server port</param>
    /// <param name="callback">Callback</param>
    public static void ReleasePort(int port, Action<Responce> callback)
    {
        ClientRequest.Request(RequestType.release_port, callback, port);
    }

    /// <summary>
    /// When room is created, need send it to match maker
    /// </summary>
    /// <param name="port">Lobby id</param>
    /// <param name="callback">Callback</param>
    public static void ServerCreated(int lobbyId, Action<Responce> callback)
    {
        ClientRequest.Request(RequestType.server_created, callback, lobbyId);
    } 

    #endregion
}
