[System.Serializable]
public class PlayerModel
{
    /// <summary>
    /// Nickname of the player
    /// </summary>
    public string Nickname;

    /// <summary>
    /// Unique Player ID
    /// </summary>
    public string Id;

    /// <summary>
    /// Client runtime platform
    /// </summary>
    public UnityEngine.RuntimePlatform Platform;
}
