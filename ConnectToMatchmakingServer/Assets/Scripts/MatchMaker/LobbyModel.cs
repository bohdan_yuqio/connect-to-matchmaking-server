using System.Collections.Generic;

public class LobbyModel : Responce
{
    /// <summary>
    /// Id lobby
    /// </summary>
    public int LobbyId;
    /// <summary>
    /// Room port
    /// </summary>
    public int Port;

    /// <summary>
    /// Lobby rank
    /// </summary>
    public int Rank;
    /// <summary>
    /// Lobby bet
    /// </summary>
    public int Bet;
    /// <summary>
    /// Lobby score
    /// </summary>
    public int Score;

    /// <summary>
    /// True - if nedded pass to connect lobby
    /// </summary>
    public bool NeededPass;
    /// <summary>
    /// True - if server create room
    /// </summary>
    public bool RoomIsCreated;

    /// <summary>
    /// Room ip
    /// </summary>
    public string Ip;

    /// <summary>
    /// List of players in lobby
    /// </summary>
    public List<PlayerModel> Players = new List<PlayerModel>();
}
