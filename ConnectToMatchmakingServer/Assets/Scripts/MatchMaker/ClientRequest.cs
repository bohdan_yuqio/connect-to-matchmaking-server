using System.Collections;
using System.Collections.Generic;
using System;
using System.Threading;
using UnityEngine;
using System.Net.Sockets;
using System.Text;
using System.Net;
using Newtonsoft.Json;

public class ClientRequest : MonoBehaviour
{
    private static ClientRequest _instance;

    private const string ADRESS = "http://127.0.0.1:10001/"; // "http://91.227.183.56:10001/" "http://127.0.0.1:10001/"

    public static ClientRequest Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<ClientRequest>();
            if (_instance == null)
            {
                GameObject temp = Instantiate(new GameObject("ClientRequest"));
                _instance = temp.AddComponent<ClientRequest>();
            }
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null)
            Destroy(this);

        DontDestroyOnLoad(gameObject);
    }

    private void ConnectToLobbyHandler(LobbyModel obj)
    {
        MatchMaker.GetLobbies(Application.platform, (res) => { });
    }

    /// <summary>
    /// Server request manager
    /// </summary>
    /// <typeparam name="T">Type of model</typeparam>
    /// <param name="request">Request that you need to send to the server.</param>
    /// <param name="callback">Callback.</param>
    /// <param name="param">An optional parameter for querying the server</param>
    public static void Request<T>(RequestType request, Action<T> callback, params object[] param)
    {
        Instance.StartCoroutine(Instance.Connect(request, callback, param));
    }

    private IEnumerator Connect<T>(RequestType request, Action<T> callback, object[] param)
    {
        using (WWW www = new WWW(ADRESS + request.ToString(), Encoding.UTF8.GetBytes(GetQuery(request, param))))
        { 
            yield return www;
            print(www.text);
            callback((T)JsonConvert.DeserializeObject(www.text, typeof(T)));
        }
    }

    /// <summary>
    /// Get server request
    /// </summary>
    private string GetQuery(RequestType request, object[] param)
    {
        switch (request)
        {
            case RequestType.find_match:
                return "{" + string.Format("\"Rank\": {0}, \"Bet\": {1}, \"Score\": {2}, \"Nickname\": \"{3}\", \"PlayerId\": \"{4}\", \"Platform\": {5}", param[0], param[1], param[2], param[3], param[4], param[5]) + "}";
            case RequestType.create_lobby:
                return "{" + string.Format("\"Rank\": {0}, \"Bet\": {1}, \"Score\": {2}, \"Nickname\": \"{3}\", \"PlayerId\": \"{4}\", \"Platform\": {5}, \"Pass\": \"{6}\"", param[0], param[1], param[2], param[3], param[4], param[5], param[6]) + "}";
            case RequestType.get_lobby_data:
                return "{" + string.Format("\"LobbyId\": {0}, \"Platform\": {1}", param[0], param[1]) + "}";
            case RequestType.get_lobbies:
                return "{" + string.Format("\"Platform\": {0}", param[0]) + "}";
            case RequestType.connect_to_lobby:
                return "{" + string.Format("\"Nickname\": \"{0}\", \"PlayerId\": \"{1}\", \"Platform\": {2}, \"LobbyId\": {3},\"Pass\": \"{4}\"", param[0], param[1], param[2], param[3], param[4]) + "}";
            case RequestType.disconnect_from_lobby:
                return "{" + string.Format("\"PlayerId\": {0}, \"LobbyId\": \"{1}\"", param[0], param[1]) + "}";

            case RequestType.release_port:
                return "{" + string.Format("\"Port\": {0}", param[0]) + "}";
            case RequestType.server_created:
                return "{" + string.Format("\"LobbyId\": {0}", param[0]) + "}";
        }
        return null;
    }
}

public enum RequestType
{
    find_match,
    get_lobbies,
    create_lobby,
    get_lobby_data,
    connect_to_lobby,
    disconnect_from_lobby,
    release_port,
    server_created
}

/// <summary>
/// Base answer from server
/// </summary>
public class Responce
{
    /// <summary>
    /// Status of sended operation
    /// </summary>
    public ResponceStatus Status;

    /// <summary>
    /// Error massage(optional, some requests dont generete massge, see docs)
    /// </summary>
    public string ErrorText;

    public enum ResponceStatus
    {
        Success,
        Error
    }
}
