﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FindGameOnMatchmakingServer : MonoBehaviour
{
    public string GameSceneName = "GameSceneNew";//same name as in real project

    public InputField nickname;
    public InputField playerId;

    public Button findMatch;

    public ConnectedPlayerView connectedPlayerPrefab;

    public ScrollRect connectedPlayers;

    public RemoteServerConnectionData connectionData;

    public void FindMatch()
    {
        findMatch.interactable = false;
        MatchMaker.FindMatch(40, 50, 100, nickname.text, playerId.text, Application.platform, GetLobbyModelHandler);
    }

    private void GetLobbyModelHandler(LobbyModel model)
    {
        if(model == null)
        {
            findMatch.interactable = true;
            return;
        }

        ShowConnectedPlayers(model.Players);

        if (model.RoomIsCreated)
        {
            connectionData.IP = model.Ip;
            connectionData.Port = model.Port;

            SceneManager.LoadScene(GameSceneName);
        }
        else
            StartCoroutine(UpdateLobbyData(model));
    }

    private IEnumerator UpdateLobbyData(LobbyModel model)
    {
        yield return new WaitForSeconds(1);

        MatchMaker.GetLobbyData(model.LobbyId, Application.platform, GetLobbyModelHandler);
    }

    private void ShowConnectedPlayers(List<PlayerModel> players)
    {
        for (int i = 0; i < connectedPlayers.content.childCount; i++)
            Destroy(connectedPlayers.content.GetChild(i).gameObject);

        for (int i = 0; i < players.Count; i++)
        {
            Instantiate(connectedPlayerPrefab, connectedPlayers.content).playerName = players[i].Nickname;
        }
    }
}
