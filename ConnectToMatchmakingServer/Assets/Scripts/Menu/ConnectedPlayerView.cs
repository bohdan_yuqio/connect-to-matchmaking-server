﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConnectedPlayerView : MonoBehaviour {

    public string playerName;

    [SerializeField]
    private Text _playerName;

	// Use this for initialization
	void Start () {
        _playerName.text = playerName;
	}

}
