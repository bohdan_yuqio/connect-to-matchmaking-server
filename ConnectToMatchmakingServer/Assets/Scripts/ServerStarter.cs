﻿using UnityEngine;
using UnityEngine.Networking;

public class ServerStarter : MonoBehaviour
{
	public NetworkManager NetworkManager;

	public int Port = 7777;

	private void Start()
	{
        var portStr = GetCommandLineArg("-port");
		if (!string.IsNullOrEmpty(portStr))
			Port = int.Parse(portStr);

		NetworkManager.networkPort = Port;
		NetworkManager.StartServer();

        var lobbyId = GetCommandLineArg("-lobbyId");
        MatchMaker.ServerCreated(int.Parse(lobbyId), (res) => { });
	}

	/// <summary>
	/// Taken from here: https://stackoverflow.com/questions/39843039/game-development-how-could-i-pass-command-line-arguments-to-a-unity-standalo
	/// Not tested.
	/// </summary>
	/// <param name="name"></param>
	/// <returns></returns>
	private static string GetCommandLineArg(string name)
	{
		var args = System.Environment.GetCommandLineArgs();
		for (int i = 0; i < args.Length; i++)
		{
			if (args[i] == name && args.Length > i + 1)
			{
				return args[i + 1];
			}
		}
		return null;
	}

    private void OnApplicationQuit()
    {
        MatchMaker.ReleasePort(Port, (res) => { });
    }
}
