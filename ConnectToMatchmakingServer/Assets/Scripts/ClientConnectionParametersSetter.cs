﻿using UnityEngine;
using UnityEngine.Networking;

public class ClientConnectionParametersSetter : MonoBehaviour {

	public NetworkManager NetworkManager;
	public RemoteServerConnectionData ConnectionData;

	private void Start()
	{
		NetworkManager.networkAddress = ConnectionData.IP;
		NetworkManager.networkPort = ConnectionData.Port;
	}
}
