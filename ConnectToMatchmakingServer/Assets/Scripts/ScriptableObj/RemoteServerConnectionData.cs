﻿using UnityEngine;

[CreateAssetMenu(fileName = "ConnectionToRemoteServerData")]
public class RemoteServerConnectionData : ScriptableObject
{
	public string IP = "localhost";
	public int Port = 7777;
}
